package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PspTutorial08Application {

	public static void main(String[] args) {
		SpringApplication.run(PspTutorial08Application.class, args);
	}
}
