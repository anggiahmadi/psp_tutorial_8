package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;
import com.example.service.UnivService;

@RestController
public class RestTugasController {
	@Autowired
	PesertaService pesertaDAO;
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	UnivService univDAO;
	
	@RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
	public PesertaModel peserta(@RequestParam(value = "nomor", required = false) String nomor)
	{
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
    	
    	if(peserta != null)
    	{
        	return peserta;
    	}
    	else
    	{
        	return null;
    	}
	}
	
	@RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
	public ProdiModel prodi(@RequestParam(value = "kode", required = false) String kodeProdi)
	{
		ProdiModel prodi = prodiDAO.selectProdi(kodeProdi);
    	
    	if(prodi != null)
    	{
        	return prodi;
    	}
    	else
    	{
        	return null;
    	}
	}
	
	@RequestMapping(value = "rest/univ/{kodeUniv}", method = RequestMethod.GET)
	public UnivModel univ(@PathVariable(value = "kodeUniv") String kodeUniv)
	{
		UnivModel univ = univDAO.selectUniv(kodeUniv);
    	
    	if(univ != null)
    	{
    		return univ;
    	}
    	else
    	{
        	return null;
    	}
	}
}
