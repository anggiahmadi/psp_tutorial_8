package com.example.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;
import com.example.service.UnivService;

@Controller
public class TugasController 
{
	@Autowired
	UnivService univDAO;
	
	@Autowired
	PesertaService pesertaDAO;
	
	@Autowired
	ProdiService prodiDAO;

    @RequestMapping("/")
    public String index (Model model)
    {
    	String linkUrl = "home";
        model.addAttribute ("linkUrl", linkUrl);
        return "index";
    }
    
    @RequestMapping("/pengumuman/submit")
    public String pengumuman(Model model, @ModelAttribute PesertaModel findPeserta)
    {
    	PesertaModel peserta = pesertaDAO.selectPeserta(findPeserta.getNomor());
    	
    	if(peserta != null)
    	{
    		Boolean isPengumuman = true;
    		String informasiPeserta = "Pengumuman SBMPTN";
    		
    		model.addAttribute("pengumuman", isPengumuman);
    		model.addAttribute("informasiPeserta", informasiPeserta);
            model.addAttribute ("peserta", peserta);
        	return "pesertaDetail";
    	}
    	else
    	{
    		String nomor = findPeserta.getNomor();
            model.addAttribute ("nomor", nomor);
        	return "not-found-peserta";
    	}
    }
    
    @RequestMapping("/univ")
    public String univ(Model model)
    {
    	String linkUrl = "univ";
    	List<UnivModel> univs = univDAO.selectAllUnivs();
    	model.addAttribute ("univs", univs);
        model.addAttribute ("linkUrl", linkUrl);
    	return "univ";
    }
    
    @RequestMapping("/univ/{kodeUniv}")
    public String univPath (Model model, @PathVariable(value = "kodeUniv") String kodeUniv)
    {
    	UnivModel univ = univDAO.selectUniv(kodeUniv);
    	
    	if(univ != null)
    	{
        	model.addAttribute ("univ", univ);
    		return "univDetail";
    	}
    	else
    	{
            model.addAttribute ("kodeUniv", kodeUniv);
        	return "not-found-univ";
    	}
    }
    
    @RequestMapping("/univ/daftarMahasiswa/{kodeUniv}")
    public String univDaftarMahasiswa(Model model, @PathVariable(value = "kodeUniv") String kodeUniv)
    {
    	UnivModel univ = univDAO.selectUniv(kodeUniv);
    	
    	if(univ != null)
    	{
        	List<PesertaModel> peserta = pesertaDAO.findPesertaByUniv(kodeUniv);
        	
        	model.addAttribute ("univ", univ);
        	model.addAttribute ("peserta", peserta);
    		return "univDaftarMahasiswa";
    	}
    	else
    	{
            model.addAttribute ("kodeUniv", kodeUniv);
        	return "not-found-univ";
    	}
    }
    @RequestMapping("/univ/tambahProdi/{kodeUniv}")
    public String univTambahProdi(ProdiModel prodi, Model model, @PathVariable(value = "kodeUniv") String kodeUniv)
    {
    	UnivModel univ = univDAO.selectUniv(kodeUniv);
    	
    	if(univ != null)
    	{
        	model.addAttribute ("prodi", prodi);
        	model.addAttribute ("univ", univ);
    		return "form-add-prodi";
    	}
    	else
    	{
            model.addAttribute ("kodeUniv", kodeUniv);
        	return "not-found-univ";
    	}
    }
    @RequestMapping("/univ/tambahProdi/submit")
    public String submitTambahProdi(HttpServletResponse response, @Valid ProdiModel prodi, BindingResult bindingResult) throws IOException
    {
    	if (bindingResult.hasErrors()) {
            return "form-add-prodi";
        }else{
        	prodi.setKodeProdi(prodi.getKodeUniv()+""+prodi.getKodeProdi());
        	prodiDAO.addProdi(prodi);
        	response.sendRedirect("/univ/"+prodi.getKodeUniv());
        }
        return "index";
    }
    
    @RequestMapping("/prodi")
    public String prodi(Model model, @RequestParam(value = "kode", required = false) String kodeProdi)
    {
    	ProdiModel prodi = prodiDAO.selectProdi(kodeProdi);
    	
    	if(prodi != null)
    	{
        	UnivModel univ = univDAO.selectUniv(prodi.getKodeUniv());
        	PesertaModel maxPeserta = pesertaDAO.maxUsiaPeserta(prodi.getKodeProdi());
        	PesertaModel minPeserta = pesertaDAO.minUsiaPeserta(prodi.getKodeProdi());

        	model.addAttribute ("maxPeserta", maxPeserta);
        	model.addAttribute ("minPeserta", minPeserta);
        	model.addAttribute ("univ", univ);
        	model.addAttribute ("prodi", prodi);
        	return "prodiDetail";
    	}
    	else
    	{
            model.addAttribute ("kodeProdi", kodeProdi);
        	return "not-found-prodi";
    	}
    }
    
    @RequestMapping("/peserta")
    public String peserta(Model model, @RequestParam(value = "nomor", required = false) String nomor)
    {
    	PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
    	
    	if(peserta != null)
    	{
    		Boolean isPengumuman = false;
    		String informasiPeserta = "Detail Peserta "+nomor;
    		
    		model.addAttribute("pengumuman", isPengumuman);
    		model.addAttribute("informasiPeserta", informasiPeserta);
            model.addAttribute ("peserta", peserta);
        	return "pesertaDetail";
    	}
    	else
    	{
            model.addAttribute ("nomor", nomor);
        	return "not-found-peserta";
    	}
    }
    
    @RequestMapping("/peserta/edit/{nomor}")
    public String pesertaEditor(Model model, @PathVariable(value = "nomor", required = false) String nomor)
    {
    	List<ProdiModel> prodis = prodiDAO.selectAllProdis();
    	PesertaModel peserta = pesertaDAO.selectPeserta(nomor);

    	model.addAttribute ("prodis", prodis);
    	model.addAttribute ("peserta", peserta);
    	return "form-edit-peserta";
    }
    
    @RequestMapping("peserta/edit/submit")
    public String submitEditPeserta(HttpServletResponse response, @ModelAttribute PesertaModel peserta) throws IOException, ParseException
    {

		if(peserta.getKodeProdi().equals("TIDAK LULUS"))
			peserta.setKodeProdi(null);
		
		DateFormat format = new SimpleDateFormat("mm-dd-yyyy", Locale.ENGLISH);
		Date date = format.parse(peserta.getTglLahirString());
		peserta.setTglLahir(date);
		
		pesertaDAO.updatePeserta(peserta);
    	response.sendRedirect("/peserta?nomor="+peserta.getNomor());
        return null;
    }
}
