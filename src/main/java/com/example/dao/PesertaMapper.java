package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.PesertaModel;

@Mapper
public interface PesertaMapper {
	
	@Select("SELECT pes.nomor, pes.nama, pes.tgl_lahir, pes.kode_prodi, pro.nama_prodi, u.nama_univ, u.url_univ FROM peserta pes LEFT JOIN prodi pro ON pes.kode_prodi = pro.kode_prodi LEFT JOIN univ u ON pro.kode_univ = u.kode_univ WHERE nomor = #{nomor}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	PesertaModel selectPeserta(String nomor);
	
	@Select("SELECT pes.nomor, pes.nama, pes.tgl_lahir, pes.kode_prodi, pro.nama_prodi, u.nama_univ, u.url_univ FROM peserta pes LEFT JOIN prodi pro ON pes.kode_prodi = pro.kode_prodi LEFT JOIN univ u ON pro.kode_univ = u.kode_univ")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	List<PesertaModel> selectAllPesertas();

	@Update("UPDATE peserta SET nama = #{nama}, tgl_lahir = #{tglLahir}, kode_prodi = #{kodeProdi} WHERE nomor = #{nomor}")
	void updatePeserta(PesertaModel peserta);
	
	@Select("SELECT pes.nomor, pes.nama, pes.tgl_lahir, pes.kode_prodi, pro.nama_prodi, u.nama_univ, u.url_univ FROM peserta pes LEFT JOIN prodi pro ON pes.kode_prodi = pro.kode_prodi LEFT JOIN univ u ON pro.kode_univ = u.kode_univ WHERE u.kode_univ = #{kodeUniv}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	List<PesertaModel> findPesertaByUniv(String kodeUniv);
	
	@Select("SELECT pes.nomor, pes.nama, pes.tgl_lahir, pes.kode_prodi, pro.nama_prodi, u.nama_univ, u.url_univ FROM peserta pes LEFT JOIN prodi pro ON pes.kode_prodi = pro.kode_prodi LEFT JOIN univ u ON pro.kode_univ = u.kode_univ WHERE pes.kode_prodi = #{kodeProdi} ORDER BY tgl_lahir ASC LIMIT 1")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	PesertaModel maxUsiaPeserta(String kodeProdi);

	@Select("SELECT pes.nomor, pes.nama, pes.tgl_lahir, pes.kode_prodi, pro.nama_prodi, u.nama_univ, u.url_univ FROM peserta pes LEFT JOIN prodi pro ON pes.kode_prodi = pro.kode_prodi LEFT JOIN univ u ON pro.kode_univ = u.kode_univ WHERE pes.kode_prodi = #{kodeProdi} ORDER BY tgl_lahir DESC LIMIT 1")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	PesertaModel minUsiaPeserta(String kodeProdi);
}
