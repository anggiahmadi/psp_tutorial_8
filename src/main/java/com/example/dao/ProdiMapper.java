package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

@Mapper
public interface ProdiMapper {
	@Select("SELECT p.*, u.nama_univ FROM prodi p INNER JOIN univ u ON p.kode_univ = u.kode_univ WHERE kode_prodi = #{kodeProdi}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="pesertas", column="kode_prodi",
					javaType = List.class,
					many=@Many(select="selectPeserta"))
	})
	ProdiModel SelectProdi(String kodeProdi);
	
	@Select("SELECT p.*, u.nama_univ FROM prodi p INNER JOIN univ u ON p.kode_univ = u.kode_univ")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi"),
			@Result(property="namaUniv", column="nama_univ")
	})
	List<ProdiModel> SelectAllProdis();
	
	@Insert("INSERT INTO prodi(kode_univ, kode_prodi, nama_prodi) VALUES (#{kodeUniv}, #{kodeProdi}, #{namaProdi})")
	void addProdi(ProdiModel prodi);
	
	@Delete("DELETE FROM prodi WHERE kode_prodi = #{kodeProdi}")
	void deleteProdi(String kodeProdi);

	@Update("UPDATE univ SET kode_univ = #{kodeUniv}, nama_prodi = #{namaProdi} WHERE kode_prodi = #{kodeProdi}")
	void update(ProdiModel prodi);
	
	@Select("SELECT * " + "FROM peserta"
			+ " WHERE kode_prodi = #{kodeProdi}")
	@Results(value = {
			@Result(property="nomor", column="nomor"),
			@Result(property="nama", column="nama"),
			@Result(property="tglLahir", column="tgl_lahir"),
			@Result(property="kode_prodi", column="kodeProdi")
	})
	List<PesertaModel> selectPeserta(@Param("kodeProdi") String kodeProdi);
}
