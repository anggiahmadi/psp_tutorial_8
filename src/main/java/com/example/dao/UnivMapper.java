package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.ProdiModel;
import com.example.model.UnivModel;

@Mapper
public interface UnivMapper {
	@Select("SELECT * FROM univ WHERE kode_univ = #{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ"),
			@Result(property="prodis", column="kode_univ",
					javaType = List.class,
					many=@Many(select="selectProdis"))
	})
	UnivModel selectUniv(String kodeUniv);
	
	@Select("SELECT * FROM univ")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	List<UnivModel> selectAllUnivs();

	@Insert("INSERT INTO univ (kode_univ, nama_univ, url_univ) VALUES (#{kodeUniv}, #{namaUniv}, #{urlUniv})")
	void addUniv(UnivModel univ);

	@Delete("DELETE FROM univ WHERE kode_univ = #{kodeUniv}")
	void deleteUniv(String kode_univ);
	
	@Update("UPDATE univ SET nama_univ = #{namaUniv}, url_univ = #{urlUniv} WHERE kode_univ = #{kodeUniv}")
	void updateUniv(UnivModel univ);

	@Select("SELECT kode_univ, kode_prodi, nama_prodi " + "FROM prodi"
			+ " WHERE kode_univ = #{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="kodeProdi", column="kode_prodi"),
			@Result(property="namaProdi", column="nama_prodi")
	})
	List<ProdiModel> selectProdis(@Param("kodeUniv") String kodeUniv);
}
