package com.example.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PesertaModel {
	private String nomor;
	@NotNull
    @Size(min=2, max=30)
	private String nama;
	@NotNull
	private Date tglLahir;
	private String tglLahirString;
	private String kodeProdi;
	private String namaProdi;
	private String namaUniv;
	private String urlUniv;

	public String getTglLahirFormat(){
		return new SimpleDateFormat("MM-dd-yyyy").format(tglLahir);
	}
	public Integer getUmur() {
		Calendar a = getCalendar(tglLahir);
	    Calendar b = getCalendar(new Date());

	    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
	    return diff;
	}
	
	public static Calendar getCalendar(Date date) {
	    Calendar cal = Calendar.getInstance(Locale.US);
	    cal.setTime(date);
	    return cal;
	}

	public Boolean isLulus() {
		if(kodeProdi == null)
			return false;
		else
			return true;
	}
}