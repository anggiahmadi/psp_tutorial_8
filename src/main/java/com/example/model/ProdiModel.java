package com.example.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
	private String kodeUniv;
	@NotNull
    @Size(min=2, max=4)
	private String kodeProdi;
	@NotNull
	private String namaProdi;
	private String namaUniv;
	private List<PesertaModel> pesertas;
}
