package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;

public interface PesertaService 
{
	PesertaModel selectPeserta(String nomor);
	
	List<PesertaModel> selectAllPesertas();
	
	void addPeserta(PesertaModel peserta);
	
	void deletePeserta(String nomor);
	
	void updatePeserta(PesertaModel peserta);
	
	List<PesertaModel> findPesertaByUniv(String kodeUniv);
	
	PesertaModel maxUsiaPeserta(String kodeProdi);
	
	PesertaModel minUsiaPeserta(String kodeProdi);
}
