package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService 
{
	@Autowired
	private PesertaMapper pesertaMapper;

	@Override
	public PesertaModel selectPeserta(String nomor) {
    	log.info ("select peserta with nomor {}", nomor);
		return pesertaMapper.selectPeserta(nomor);
	}

	@Override
	public List<PesertaModel> selectAllPesertas() {
    	log.info ("select all peserta");
		return pesertaMapper.selectAllPesertas();
	}

	@Override
	public void addPeserta(PesertaModel peserta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePeserta(String nomor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePeserta(PesertaModel peserta) {
		log.info("peserta "+peserta.getNomor()+" is updated");
		pesertaMapper.updatePeserta(peserta);
	}

	@Override
	public List<PesertaModel> findPesertaByUniv(String kodeUniv) {
    	log.info ("find peserta by kodeUniv "+kodeUniv);
		return pesertaMapper.findPesertaByUniv(kodeUniv);
	}

	@Override
	public PesertaModel maxUsiaPeserta(String kodeProdi) {
		log.info ("max usia peserta by kodeProdi "+kodeProdi);
		return pesertaMapper.maxUsiaPeserta(kodeProdi);
	}

	@Override
	public PesertaModel minUsiaPeserta(String kodeProdi) {
		log.info ("min usia peserta by kodeProdi "+kodeProdi);
		return pesertaMapper.minUsiaPeserta(kodeProdi);
	}

}
