package com.example.service;

import java.util.List;

import com.example.model.ProdiModel;

public interface ProdiService 
{
	ProdiModel selectProdi(String kodeProdi);
	
	List<ProdiModel> selectAllProdis();
	
	void addProdi(ProdiModel prodi);
	
	void deleteProdi(String kodeProdi);
	
	void update(ProdiModel prodi);
}
