package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.ProdiModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService 
{
	@Autowired
	private ProdiMapper prodiMapper;
	
	@Override
	public ProdiModel selectProdi(String kodeProdi) {
    	log.info ("select prodi with kode_prodi {}", kodeProdi);
		return prodiMapper.SelectProdi(kodeProdi);
	}

	@Override
	public List<ProdiModel> selectAllProdis() {
		log.info("select all prodis");
		return prodiMapper.SelectAllProdis();
	}

	@Override
	public void addProdi(ProdiModel prodi) {
		log.info("add prodi");
		prodiMapper.addProdi(prodi);
	}

	@Override
	public void deleteProdi(String kodeProdi) {
		log.info("prodi "+kodeProdi+" is deleted");
		prodiMapper.deleteProdi(kodeProdi);
	}

	@Override
	public void update(ProdiModel prodi) {
		log.info("prodi "+prodi.getKodeProdi()+" is updated");
		prodiMapper.update(prodi);
	}

}
