package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UnivMapper;
import com.example.model.UnivModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService 
{
	@Autowired
	private UnivMapper univMapper; 
	
	@Override
	public UnivModel selectUniv(String kodeUniv) {
    	log.info ("select univ with kode_univ {}", kodeUniv);
        return univMapper.selectUniv(kodeUniv);
	}

	@Override
	public List<UnivModel> selectAllUnivs() {
		log.info("select all univs");
		return univMapper.selectAllUnivs();
	}

	@Override
	public void addUniv(UnivModel univ) {
		log.info("add univ");
		univMapper.addUniv(univ);
	}

	@Override
	public void deleteUniv(String kodeUniv) {
		log.info("univ "+kodeUniv+" is deleted");
		univMapper.deleteUniv(kodeUniv);
	}

	@Override
	public void updateUniv(UnivModel univ) {
		log.info("univ "+univ.getKodeUniv()+" is updated");
		univMapper.updateUniv(univ);
	}

}
